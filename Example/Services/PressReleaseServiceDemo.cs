﻿using DD4T.ContentModel.Exceptions;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using Example.Models.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tridion.ContentDelivery.DynamicContent.Query;

namespace Example.Services
{
    public class PressReleaseService
    {
        IViewModelFactory _viewModelFactory;
        IComponentPresentationFactory _componentPresentationFactory;
        public PressReleaseService(IViewModelFactory viewModelFactory, IComponentPresentationFactory componentPresentationFactory)
        {
            this._viewModelFactory = viewModelFactory;
            this._componentPresentationFactory = componentPresentationFactory;
        }
        public IList<PressRelease> GetAllPressReleases()
        {
            Query query = new Query();
            query.Criteria = new SchemaTitleCriteria("Press Release");
            IList<PressRelease> pressReleases = new List<PressRelease>();
            foreach (var uri in query.ExecuteQuery())
            {
                try
                {
                    var cp = _componentPresentationFactory.GetComponentPresentation(uri);
                    if (cp == null)
                    {
                        // TODO: log 
                        continue;
                    }
                    PressRelease press = _viewModelFactory.BuildViewModel<PressRelease>(cp);
                    pressReleases.Add(press);
                }
                catch (ComponentPresentationNotFoundException e)
                {
                    // TODO: log
                }
            }
            return pressReleases;
        }

        public IList<PressRelease> GetPressReleases(string type)
        {
            
            Query query = new Query();
            query.Criteria = new SchemaTitleCriteria("Press Release");
            query.Criteria =  new CategorizationCriteria(8, "Press Release Type", new[] { type });


           
            IList<PressRelease> pressReleases = new List<PressRelease>();
            foreach (var uri in query.ExecuteQuery())
            {
                try
                {
                    var cp = _componentPresentationFactory.GetComponentPresentation(uri);
                    if (cp == null)
                    {
                        // TODO: log 
                        continue;
                    }
                    PressRelease press = _viewModelFactory.BuildViewModel<PressRelease>(cp);
                    pressReleases.Add(press);
                }
                catch (ComponentPresentationNotFoundException e)
                {
                    // TODO: log
                }
            }
            return pressReleases;
        }


    }

}