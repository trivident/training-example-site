﻿using DD4T.ContentModel.Exceptions;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using DD4T.Factories;
using DD4T.ViewModels;
using Example.Contracts.Services;
using Example.Models.Component;
using System.Collections.Generic;
using Tridion.ContentDelivery.DynamicContent.Query;

namespace Example.Services
{
    public class FeatureService : IFeatureService
    {
        IViewModelFactory _viewModelFactory;
        IComponentPresentationFactory _componentPresentationFactory;
        public FeatureService(IViewModelFactory viewModelFactory, IComponentPresentationFactory componentPresentationFactory)
        {
            this._viewModelFactory = viewModelFactory;
            this._componentPresentationFactory = componentPresentationFactory;
        }
        public IList<Feature> GetAllFeatures()
        {
            Query query = new Query();
            query.Criteria = new SchemaTitleCriteria("Feature");
            IList<Feature> features = new List<Feature>();
            foreach (var uri in query.ExecuteQuery())
            {
                try
                {
                    var cp = _componentPresentationFactory.GetComponentPresentation(uri);
                    if (cp == null)
                    {
                        // TODO: log 
                        continue;
                    }
                    Feature feature = _viewModelFactory.BuildViewModel<Feature>(cp);
                    features.Add(feature);
                }
                catch (ComponentPresentationNotFoundException e)
                {
                    // TODO: log
                }
            }
            return features;
        }
    }
}