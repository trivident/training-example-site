﻿using System;
using System.Collections.Generic;
using System.Linq;
using DD4T.ContentModel.Contracts.Logging;
using Example.Models.Sitemap;
using System.Runtime.Caching;
using DD4T.ContentModel.Factories;
using System.Configuration;
using DD4T.Core.Contracts.ViewModels;
using Example.Models.Labels;
using Example.Contracts.Services;

namespace Example.Services
{
    public class LabelService : ILabelService
    {
        private readonly ILogger _logger;
        private readonly IPageFactory _pageFactory;
        private readonly IViewModelFactory _viewModelFactory;

        public LabelService(ILogger logger, IPageFactory pageFactory, IViewModelFactory viewModelFactory)
        {            
            this._logger = logger;
            _pageFactory = pageFactory;
            _viewModelFactory = viewModelFactory;
        }

        private int CacheExpiryInSeconds => 30;

        public LabelSet Labels
        {
            get
            {
                LabelSet labels = (LabelSet)MemoryCache.Default["Labels"];
                if (labels == null)
                {
                    labels = new LabelSet();
                    labels.Labels = new List<Label>();
                    var page = _pageFactory.FindPage(ConfigurationManager.AppSettings["Trivident.LabelsUrl"]);
                    foreach (var cp in page.ComponentPresentations)
                    {
                        LabelSet subset = _viewModelFactory.BuildViewModel<LabelSet>(cp);
                        labels.Labels.AddRange(subset.Labels);
                    }
                    MemoryCache.Default.Add("Labels", labels, 
                    new CacheItemPolicy()
                    {
                        AbsoluteExpiration = new DateTimeOffset(DateTime.UtcNow.AddSeconds(CacheExpiryInSeconds))
                    });
                }
                return labels;                
            }
        } 
    }
}
