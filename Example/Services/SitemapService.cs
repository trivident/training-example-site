﻿using System;
using System.Collections.Generic;
using System.Linq;
using DD4T.ContentModel.Contracts.Logging;
using Example.Models.Sitemap;
using System.Runtime.Caching;
using DD4T.ContentModel.Factories;
using System.Configuration;
using DD4T.Core.Contracts.ViewModels;

namespace Example.Services
{
    public class SitemapService 
    {
        private readonly ILogger _logger;
        private readonly IPageFactory _pageFactory;
        private readonly IViewModelFactory _viewModelFactory;

        public SitemapService(ILogger logger, IPageFactory pageFactory, IViewModelFactory viewModelFactory)
        {            
            this._logger = logger;
            _pageFactory = pageFactory;
            _viewModelFactory = viewModelFactory;
        }

        private int CacheExpiryInSeconds => 30;

        public Sitemap Sitemap
        {
            get
            {
                Sitemap sitemap = (Sitemap)MemoryCache.Default["Sitemap"];
                if (sitemap == null)
                {
                    var page = _pageFactory.FindPage(ConfigurationManager.AppSettings["Trivident.SitemapUrl"]);
                    sitemap = _viewModelFactory.BuildViewModel<Sitemap>(page);
                    MemoryCache.Default.Add("Sitemap", sitemap, 
                    new CacheItemPolicy()
                    {
                        AbsoluteExpiration = new DateTimeOffset(DateTime.UtcNow.AddSeconds(CacheExpiryInSeconds))
                    });
                }
                return sitemap;                
            }
        } 
    }
}
