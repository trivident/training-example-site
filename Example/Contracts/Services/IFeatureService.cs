﻿using DD4T.ContentModel.Exceptions;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using DD4T.Factories;
using DD4T.ViewModels;
using Example.Models.Component;
using System.Collections.Generic;
using Tridion.ContentDelivery.DynamicContent.Query;

namespace Example.Contracts.Services
{
    public interface IFeatureService
    {
        IList<Feature> GetAllFeatures();
    }
}