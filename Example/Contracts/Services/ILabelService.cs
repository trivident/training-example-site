﻿using System;
using System.Collections.Generic;
using System.Linq;
using DD4T.ContentModel.Contracts.Logging;
using Example.Models.Sitemap;
using System.Runtime.Caching;
using DD4T.ContentModel.Factories;
using System.Configuration;
using DD4T.Core.Contracts.ViewModels;
using Example.Models.Labels;

namespace Example.Contracts.Services
{
    public interface ILabelService 
    {      
        LabelSet Labels { get; }
    }
}
