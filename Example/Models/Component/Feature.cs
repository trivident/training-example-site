﻿using DD4T.Core.Contracts.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DD4T.ContentModel;
using DD4T.Mvc.ViewModels.Attributes;
using DD4T.ViewModels.Attributes;
using System.Web.Mvc;
using DD4T.ViewModels.Base;

namespace Example.Models.Component
{
    [ContentModel("feature", true)]
    public class Feature : ViewModelBase, IRenderableViewModel
    {
        [TextField]
        public string Name { get; set; }

        [TextField]
        public string Heading { get; set; }

        [RichTextField]
        public MvcHtmlString Text { get; set; }

        [MultimediaUrl]
        public string SmallImage { get; set; }

        [MultimediaUrl]
        public string LargeImage { get; set; }

        [RenderData]
        public IRenderData RenderData { get; set; }
    }
}