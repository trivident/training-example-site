﻿using DD4T.Core.Contracts.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DD4T.ContentModel;
using DD4T.Mvc.ViewModels.Attributes;
using DD4T.ViewModels.Attributes;
using System.Web.Mvc;
using DD4T.ViewModels.Base;

namespace Example.Models.Component
{
    [ContentModel("cta", true)]
    public class CTA : ViewModelBase, IRenderableViewModel
    {
        [TextField]
        public string Heading { get; set; }

        [RichTextField]
        public MvcHtmlString Text { get; set; }

        [EmbeddedSchemaField]
        public List<Link> Links { get; set; }

        [TextField]
        public string Quote { get; set; }

        [TextField]
        public string QuoteAuthor { get; set; }

        [RenderData]
        public IRenderData RenderData { get; set; }
    }
}