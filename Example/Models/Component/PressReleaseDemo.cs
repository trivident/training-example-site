﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.ViewModels.Attributes;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Example.Models.Component
{
    [ContentModel("pressRelease", true)]
    public class PressRelease : ViewModelBase, IRenderableViewModel
    {
        [TextField]
        public string Heading { get; set; }

        [TextField]
        public string Summary { get; set; }

        [RichTextField]
        public MvcHtmlString Body { get; set; }

        [DateField]
        public DateTime ReleaseDate { get; set; }

        [KeywordTitleField(IsMetadata = true)]
        public string Type { get; set; }

        [RenderData]
        public IRenderData RenderData { get; set; }
    }
}