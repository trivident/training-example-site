﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example.Models.Component
{
    [ContentModel("link", true)]
    public class Link : ViewModelBase
    {
        [TextField]
        public string LinkText { get; set; }
        
        [ResolvedUrlField(FieldName = "link")]
        public string Internal { get; set; }

        [TextField]
        public string External { get; set; }

        public string ResolvedUrl
        {
            get
            {
                if (! string.IsNullOrEmpty(Internal))
                {
                    return Internal;
                }
                return External;
            }
        }

    }
}