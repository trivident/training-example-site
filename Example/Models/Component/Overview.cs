﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.ViewModels.Attributes;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example.Models.Component
{
    [ContentModel("overview", true)]
    public class Overview : ViewModelBase, IRenderableViewModel
    {
        [RenderData]
        public IRenderData RenderData { get; set; }

    }
}