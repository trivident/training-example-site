﻿using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example.Models.Labels
{
    [ContentModel("label", false)]
    public class Label : ViewModelBase
    {
        [TextField]
        public string Name { get; set; }
        [TextField]
        public string Value { get; set; }
    }
}