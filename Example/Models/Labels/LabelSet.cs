﻿using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example.Models.Labels
{
    [ContentModel("labelSet", false)]
    public class LabelSet : ViewModelBase
    {
        [EmbeddedSchemaField(FieldName = "label")]
        public List<Label> Labels { get; set; }

        public string GetLabel(string name)
        {
            return Labels?.Where(l => l.Name == name).FirstOrDefault()?.Value;
        }
    }
}