﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example.Models.Page
{
    [PageViewModel(TemplateTitle = "Generic")]
    public class Generic : ViewModelBase
    {
        [PageTitle]
        public string PageTitle { get; set; }

        [TextField(IsMetadata = true)]
        public List<string> Keywords { get; set; }

        [ComponentPresentations]
        public List<IRenderableViewModel> Items
        {
            get; set;
        }
    }
}