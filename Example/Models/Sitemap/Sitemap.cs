﻿using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;

namespace Example.Models.Sitemap
{
    [PageViewModel(ViewModelKeys= new string[] { "sitemap" })]
    public class Sitemap : ViewModelBase
    {
        [ComponentPresentations]
        public List<NavigationRoot> Items { get; set; }

        public NavigationRoot Root
        {
            get
            {
                if (Items == null || Items.Count == 0)
                {
                    throw new Exception("Sitemap is empty or not available");
                }
                return Items[0];
            }
        }
    }
}