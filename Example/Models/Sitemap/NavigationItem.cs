﻿using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example.Models.Sitemap
{
    public class NavigationItem : ViewModelBase
    {
        [TextField]
        public string Id { get; set; }

        [TextField]
        public string Title { get; set; }

        [TextField]
        public string Url { get; set; }

        [DateField]
        public DateTime LastMod { get; set; }

        [TextField]
        public string ChangeFrequency { get; set; }

        [TextField]
        public string Priority { get; set; }

        [TextField]
        public string MetaRobot { get; set; }

        [EmbeddedSchemaField]
        public List<NavigationItem> ChildItems { get; set; }
       
    }
}