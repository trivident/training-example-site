﻿using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Example.Models.Sitemap
{
    [ContentModel("sitemap", true)]
    public class NavigationRoot : ViewModelBase
    {
        [EmbeddedSchemaField]
        public List<NavigationItem> ChildItems { get; set; }
    }
}