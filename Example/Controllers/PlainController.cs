﻿using DD4T.ContentModel.Contracts.Configuration;
using DD4T.ContentModel.Contracts.Logging;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Example.Controllers
{
    public class PlainController : Controller
    {
        private IPageFactory _pageFactory;
        private IViewModelFactory _viewModelFactory;
        public PlainController(IPageFactory pageFactory, IViewModelFactory viewModelFactory)
        {
            _pageFactory = pageFactory;
            _viewModelFactory = viewModelFactory;
        }
        public ActionResult FindPage()
        {
            var page = _pageFactory.FindPage("/en/index.html");
            return View("myview", page);
        }
    }
}