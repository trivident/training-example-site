﻿using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Example.Controllers
{
    public class ViewModelController : Controller
    {
        private IViewModelFactory _viewModelFactory;
        private IComponentPresentationFactory _componentPresentationFactory;
        public ViewModelController(IComponentPresentationFactory componentPresentationFactory, IViewModelFactory viewModelFactory)
        {
            _componentPresentationFactory = componentPresentationFactory;
            _viewModelFactory = viewModelFactory;
        }

        public ActionResult TestModel()
        {
            var cp = _componentPresentationFactory.GetComponentPresentation("tcm:8-127");
            var model = _viewModelFactory.BuildViewModel(cp);
            return View("myview", cp);
        }
    }
}