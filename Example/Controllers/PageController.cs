﻿using DD4T.ContentModel.Contracts.Configuration;
using DD4T.ContentModel.Contracts.Logging;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.Controllers;
using Example.Contracts.Services;
using Example.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Example.Controllers
{
    public class PageController : ModelControllerBase
    {
        private SitemapService _sitemapService;
        private ILabelService _labelService;
        public PageController(IPageFactory pageFactory,
            IComponentPresentationFactory componentPresentationFactory, ILogger logger,
            IDD4TConfiguration configuration, IViewModelFactory viewModelFactory)
        : base(pageFactory, componentPresentationFactory, logger, configuration, viewModelFactory)
        {
            _sitemapService = new SitemapService(logger, pageFactory, viewModelFactory);
            _labelService = new LabelService(logger, pageFactory, viewModelFactory);
        }

        public override ActionResult PageModel(string url)
        {
            ViewBag.Sitemap = _sitemapService.Sitemap;
            ViewBag.Labels = _labelService.Labels;
            return base.PageModel(url);
        }
    }
}