﻿using DD4T.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DD4T.ContentModel.Contracts.Configuration;
using DD4T.ContentModel.Contracts.Logging;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;

namespace Example.Controllers
{
    public class ComponentController : ModelControllerBase
    {
        public ComponentController(IPageFactory pageFactory, IComponentPresentationFactory componentPresentationFactory, ILogger logger, IDD4TConfiguration dd4tConfiguration, IViewModelFactory viewModelFactory) : base(pageFactory, componentPresentationFactory, logger, dd4tConfiguration, viewModelFactory)
        {
        }
        public override ActionResult ComponentModel()
        {
            return base.ComponentModel();
        }
    }
}