﻿using DD4T.ContentModel.Contracts.Configuration;
using DD4T.ContentModel.Contracts.Logging;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.Controllers;
using Example.Models.Component;
using Example.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Example.Controllers
{
    public class PressReleaseController : Controller
    {
        private readonly IComponentPresentationFactory _componentPresentationFactory;
        private readonly IViewModelFactory _viewModelFactory;
        // GET: Press
        public PressReleaseController(IComponentPresentationFactory componentPresentationFactory, 
                                IViewModelFactory viewModelFactory) 
          
        {
            _viewModelFactory = viewModelFactory;
            _componentPresentationFactory = componentPresentationFactory;
        }
        public ActionResult Overview()
        {
            var pressOverview = RouteData.Values["model"] as Overview;
            var service = new PressReleaseService(_viewModelFactory, _componentPresentationFactory);
            //Todo: write logic to retrieve components
            var model = service.GetPressReleases("Announcment12");
            return View(GetView(), model);
        }

        protected string GetView()
        {
            return RouteData.Values["view"] as string;
        }
    }
}
