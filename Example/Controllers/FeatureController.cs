﻿using DD4T.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DD4T.ContentModel.Contracts.Configuration;
using DD4T.ContentModel.Contracts.Logging;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using Example.Services;
using DD4T.ContentModel;
using Example.Models.Component;
using Example.Contracts.Services;

namespace Example.Controllers
{
    public class FeatureController : Controller
    {
        private ILogger _logger;
        private IDD4TConfiguration _DD4TConfiguration;
        private IViewModelFactory _viewModelFactory;
        private IComponentPresentationFactory _componentPresentationFactory;
        private IFeatureService _featureService;

        public FeatureController(ILogger logger, IDD4TConfiguration dd4tConfiguration, IComponentPresentationFactory componentPresentationFactory, IViewModelFactory viewModelFactory) 
        {
            _logger = logger;
            _DD4TConfiguration = dd4tConfiguration;
            _viewModelFactory = viewModelFactory;
            _componentPresentationFactory = componentPresentationFactory;
            _featureService = new FeatureService(_viewModelFactory, _componentPresentationFactory);

        }
        public ActionResult Overview()
        {
            CTA featureOverview = RouteData.Values["model"] as CTA;
            ViewBag.Heading = featureOverview.Heading;
            var features = _featureService.GetAllFeatures(); 
            return View(GetView(), features);
        }

        protected string GetView()
        {
            return RouteData.Values["view"] as string;
        }
    }
}