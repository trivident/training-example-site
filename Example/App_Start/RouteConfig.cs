﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Example
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.MapRoute("exercise", "exercise", new { controller = "ViewModel", action = "TestModel" });

            routes.MapRoute(
                name: "TridionPage",
                url: "{*url}",
                defaults:
                new { controller = "Page", action = "PageModel" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            // routes.MapRoute(
            //    name: "TridionPage",
            //    url: "{*url}",
            //    defaults: new { controller = "Plain", action = "FindPage" }
            //);

        }
    }
}
